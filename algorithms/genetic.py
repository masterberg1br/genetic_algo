import configparser
from random import choices, random, randrange
from statistics import mean
import csv


from models.thing import item

config = configparser.ConfigParser()
config.read("parameters.ini")
weight_limit: int = int(config["DEFAULT"]["weight_limit"])
population_size: int = int(config["genetic"]["population_size"])
# probability: float = float(config["genetic"]["probability"])
generation_limit: int = int(config["genetic"]["generation_limit"])
number_of_mutations: int = int(config["genetic"]["number_of_mutations"])


def generate_population(length: int) -> list[list[bool]]:
    return [choices([True, False], k=length) for _ in range(population_size)]


def crossover(
    parent_a: list[bool], parent_b: list[bool], length: int
) -> tuple[list[bool], list[bool]]:
    pointer = randrange(1, length)
    return (
        parent_a[0:pointer] + parent_b[pointer:],
        parent_b[0:pointer] + parent_a[pointer:],
    )


def mutation(probability: float, genome: list[bool]) -> list[bool]:
    for _ in range(number_of_mutations):
        index = randrange(len(genome))
        if random() <= probability:
            genome[index] = bool(abs(genome[index] - 1))
    return genome


def selection_pair(
    population: list[list[bool]], dataset: list[item]
) -> list[list[bool]]:
    return choices(
        population=population,
        weights=[fitness(genome, items=dataset) for genome in population],
        k=2,
    )


def population_fitness(population: list[list[bool]], dataset: list[item]) -> float:
    return mean(fitness(genome, dataset) for genome in population)


def sort_population(
    population: list[list[bool]], dataset: list[item]
) -> list[list[bool]]:
    return sorted(
        population, key=lambda genome: fitness(genome, items=dataset), reverse=True
    )


def write_stats(
    population: list[list[bool]],
    dataset: list[item],
    generation_number: int,
    probability: float,
):
    file: str = f"{probability}.csv"
    with open(file, mode="a") as csv_file:
        fieldnames = ["generation", "fitness"]
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        for genome in population:
            writer.writerow(
                {
                    "generation": f"{generation_number}",
                    "fitness": f"{fitness(genome, dataset) / 1310 * 100}",
                }
            )

    # print((f"Population: {population}"))
    # print(f"Avg. Fitness:{population_fitness(population, dataset)}")
    # print(f"Best: {population[0]} ({fitness(population[0], dataset)})")
    # print(f"Worst: {population[-1]} ({fitness(population[-1], dataset)})")


def fitness(genome: list[bool], items: list[item]) -> int:
    """
    Fitness [summary].
    [extended_summary]
    Args:
        genome (list[bool]): [description]
        things (list[item]): [description]
    Raises:
        ValueError: [description]
    Returns:
        int: [description]
    """
    total_weight: int = 0
    total_value: int = 0
    for i, thing in enumerate(items):
        if genome[i] is True:
            total_weight += thing.weight
            total_value += thing.value
            if total_weight > weight_limit:
                return 0
    return total_value


def run_evolution(probability: float, dataset: list[item]) -> list[bool]:
    length: int = len(dataset)
    population: list[list[bool]] = generate_population(length=length)
    file: str = f"{probability}.csv"
    with open(file, mode="a") as csv_file:
        fieldnames = ["generation", "fitness"]
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
    for generation_number in range(generation_limit):
        population: list[list[bool]] = sort_population(population, dataset)
        write_stats(population, dataset, generation_number, probability)
        next_generation: list[list[bool]] = population[0:2]
        for _ in range(int(len(population) / 2) - 1):
            parents = selection_pair(population, dataset)
            offspring_a, offspring_b = crossover(
                parent_a=parents[0], parent_b=parents[1], length=length
            )
            next_generation += [
                mutation(probability, offspring_a),
                mutation(probability, offspring_b),
            ]
        population = next_generation
    return population[0]
