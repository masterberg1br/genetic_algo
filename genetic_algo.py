"""
[summary].
    [extended_summary]
"""
import sys

from algorithms import genetic
from models.thing import item

dataset = [
    item("Mentos", 5, 25),
    item("Meias", 10, 38),
    item("Lenços", 15, 80),
    item("Celular", 500, 200),
    item("Boné", 100, 70),
    item("Laptop", 500, 2200),
    item("Headphones", 150, 160),
    item("Caneca", 60, 350),
    item("Caderno", 40, 333),
    item("Garrafa", 30, 192),
]

genome = genetic.run_evolution(dataset=dataset, probability=float(sys.argv[1]))

print(genome)
things = [thing for i, thing in enumerate(dataset) if genome[i] is True]
print(f"Items: {str([t.name for t in things])}")
print(f"Value {sum(t.value for t in things)}")
print(f"Weight: {sum(p.weight for p in things)}")
print(genetic.fitness(genome, dataset) / 1310 * 100)

# print(f"{generations}\t|\t{(fitness(population[0])/2980*100):.2f}%\t|\t{genetic.genome_to_string(population[0])}")

# peso = 2980
# valor = 1310
