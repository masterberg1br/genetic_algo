"""
[summary].

[extended_summary]
"""
from typing import NamedTuple


class item(NamedTuple):
    """
    item [summary].

    [extended_summary]
    Args:
        NamedTuple ([type]): [description]
    """

    name: str
    value: int
    weight: int
