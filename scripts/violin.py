# libraries & dataset
import seaborn as sns
import matplotlib.pyplot as plt
import pandas

# set a grey background (use sns.set_theme() if seaborn version 0.11.0 or above)

sns.set(style="darkgrid")
file: str = "1.0"
file_csv: str = f"{file}.csv"
file_png: str = f"{file}.png"
df = pandas.read_csv(file_csv)

# Grouped violinplot
sns.violinplot(
    x="generation",
    y="fitness",
    data=df,
    palette="Pastel1",
)
plt.savefig(file_png)
